% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/zzz.R
\name{non-exported}
\alias{non-exported}
\alias{splice}
\alias{sourcepart}
\alias{unloadall}
\alias{uses.installed}
\title{Non-exported functions}
\description{
These functions aren't exported because they are largely superseded by/deprecated in favor of better ones (often in \pkg{\link[base:base-package]{base}}).
}
\section{Function Help}{

\itemize{
\item \code{\link[=splice-function]{splice}}
\item \code{\link[=sourcepart-function]{sourcepart}}
\item \code{\link[=unloadall-function]{unloadall}}
\item \code{\link[=uses.installed-function]{uses.installed}}
}
}

