Package 'pooraniRpackage'
================
January 18, 2023


```
Type: Package
Package: poorani
Title: Poorani's Helpful Functions
Version: 1.1
Author: Poorani Subramanian <349091-pooranis@users.noreply.gitlab.com>
    [aut, cre]
Description: These functions used to be in my .Rprofile, but I realized it
    would be better to put them in a package.
License: MIT + file LICENSE
URL: https://gitlab.com/pooranis/pooraniRpackage
Imports:
    gtools,
    stringr
Suggests:
    data.table
Config/testthat/edition: 3
Encoding: UTF-8
LazyData: true
Roxygen: list(old_usage = TRUE)
RoxygenNote: 7.2.1
```


#  R topics documented:
- <a href="#exported" id="toc-exported">Exported</a>
  - <a href="#dontrun" id="toc-dontrun"><code>dontrun</code></a>
  - <a href="#format_si" id="toc-format_si"><code>format_si</code></a>
  - <a href="#getwhich" id="toc-getwhich"><code>getwhich</code></a>
  - <a href="#lsnof" id="toc-lsnof"><code>lsnof</code></a>
  - <a href="#mergerec" id="toc-mergerec"><code>merge.rec</code></a>
  - <a href="#mingtzero" id="toc-mingtzero"><code>mingtzero</code></a>
  - <a href="#mixedarrange"
    id="toc-mixedarrange"><code>mixedarrange</code></a>
  - <a href="#namedlist" id="toc-namedlist"><code>namedlist</code></a>
  - <a href="#printdebug" id="toc-printdebug"><code>printdebug</code></a>
  - <a href="#revcomp" id="toc-revcomp"><code>revcomp</code></a>
  - <a href="#rmall" id="toc-rmall"><code>rmall</code></a>
  - <a href="#setclasses" id="toc-setclasses"><code>setclasses</code></a>
  - <a href="#tdf" id="toc-tdf"><code>tdf</code></a>
  - <a href="#vswitch" id="toc-vswitch"><code>vswitch</code></a>
  - <a href="#writetabtable"
    id="toc-writetabtable"><code>write.tab.table</code></a>
- <a href="#internal" id="toc-internal">Internal</a>
  - <a href="#meltdt" id="toc-meltdt"><code>meltdt</code></a>
  - <a href="#non-exported"
    id="toc-non-exported"><code>non-exported</code></a>
  - <a href="#poorani-package"
    id="toc-poorani-package"><code>poorani-package</code></a>
  - <a href="#sourcepart-function"
    id="toc-sourcepart-function"><code>sourcepart-function</code></a>
  - <a href="#splice-function"
    id="toc-splice-function"><code>splice-function</code></a>
  - <a href="#stop_quietly"
    id="toc-stop_quietly"><code>stop_quietly</code></a>
  - <a href="#unloadall-function"
    id="toc-unloadall-function"><code>unloadall-function</code></a>
  - <a href="#usesinstalled-function"
    id="toc-usesinstalled-function"><code>uses.installed-function</code></a>

# Exported

## `dontrun`

Don't run this expression

### Description

Wrapper around [`local`](#local) and [`eval.parent`](#evalparent) that
only executes if `please_run` is `TRUE` . So, can source a script
without running something terrible, but don't have to comment out
everything (which hurts readability in rmarkdown.)

### Usage

``` r
dontrun(EXPR, please_run = getOption("please_run", default = FALSE),
  .local = T)
```

### Arguments

| Argument     | Description                                                                                                                 |
|--------------|-----------------------------------------------------------------------------------------------------------------------------|
| `EXPR`       | expression not to run                                                                                                       |
| `please_run` | logical. should you run `EXPR` ? default FALSE. Can also set R [`options`](#options) to run expressions globally in script. |
| `.local`     | logical. Run `EXPR` in local environment? default TRUE.                                                                     |

### Value

nothing unless `please_run` is `TRUE` AND `EXPR` returns something.

### Examples

``` r
a <- 77
b <- 5

dontrun({
c <- a + b
cat(c, '\n')
}, .local=T, please_run = T)
## 82
ls()
##  [1] "a"                "author"           "b"                "clean"            "date"             "github_format"   
##  [7] "include_before"   "input"            "knitr_opts_chunk" "man_file"         "newlink"          "no_yaml"         
## [13] "nocodelinks"      "oldoptschunk"     "outdir"           "pat"              "pkg"              "rmd_man_file"    
## [19] "temprmd"          "title"            "toc"              "toc_depth"        "toc_title"        "toplinks"        
## [25] "v1"               "v2"               "wd"
```

## `format_si`

Format number according to SI units.

### Usage

``` r
format_si(x, ...)
```

### Arguments

| Argument | Description                                                                                               |
|----------|-----------------------------------------------------------------------------------------------------------|
| `x`      | a numeric vector                                                                                          |
| `...`    | other args to pass to [format](https://www.rdocumentation.org/packages/base/versions/3.6.0/topics/format) |

### Details

Formats numeric values according to the International System of Units.

### Value

formatted character vector

### References

"Metric Prefix." *Wikipedia*, May 16, 2019. Accessed May 26, 2019.
<https://en.wikipedia.org/w/index.php?title=Metric_prefix&oldid=897402694>.

"Metric Prefixes for Ggplot2 Scales." *BlogNotes to Myself*. Last
modified October 7, 2011. Accessed May 26, 2019.
<http://www.moeding.net/2011/10/metric-prefixes-for-ggplot2-scales/>.

## `getwhich`

Get the which values

### Description

Instead of writing
`longvariablename[which(somelogicalexpres(longvariablename))]` , write
`getwhich(longvariablename, somelogicalexpres(x))` . Like doing
`grep(pattern, vector, value=TRUE)` for more general which logical
expressions.

### Usage

``` r
getwhich(x, expr)
```

### Arguments

| Argument | Description                                                             |
|----------|-------------------------------------------------------------------------|
| `x`      | vector                                                                  |
| `expr`   | expression that must use variable name `x` that evaluates to a logical. |

### Value

values (not indexes) in x that evaluate to `TRUE` in `expr` .

### Examples

``` r
longvariablename <- 1:33
longvariablename[which(longvariablename < 5)]
## [1] 1 2 3 4
getwhich(longvariablename, x < 5)
## [1] 1 2 3 4
```

## `lsnof`

List non-functions

### Description

like [`base::ls()`](#base::ls()) or
[`utils::ls.str()`](#utils::ls.str()) except don't return any names of
functions.

### Usage

``` r
lsnof(name, all.names = FALSE, pattern, nolist = FALSE, str = FALSE,
  print = str, ...)
```

### Arguments

| Argument    | Description                                                                                                                                                                                                                              |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `name`      | which environment to use in listing the available objects. Defaults to the *current* environment. Although called `name` for back compatibility, in fact this argument can specify the environment in any form; see the Details section. |
| `all.names` | a logical value. If `TRUE` , all object names are returned. If `FALSE` , names which begin with a . are omitted.                                                                                                                         |
| `pattern`   | an optional [regular expression](#regular_expression) . Only names matching `pattern` are returned. [`glob2rx`](#glob2rx) can be used to convert wildcard patterns to regular expressions.                                               |
| `nolist`    | logical should we also not return lists?                                                                                                                                                                                                 |
| `str`       | logical. return object of type "ls_str" (output of [`ls.str`](#lsstr) ) instead of character vector (output of [`ls`](#ls) )                                                                                                             |
| `print`     | logcial. print "ls_str" object. set to be same as `str`                                                                                                                                                                                  |
| `...`       | parameters to pass to [`print.ls_str`](#printls_str) . only used if `print` is `TRUE` . default params are `max.level=0` which will only print object names and mode                                                                     |

### Details

The `name` argument can specify the environment from which object names
are taken in one of several forms: as an integer (the position in the
[`search`](#search) list); as the character string name of an element in
the search list; or as an explicit [`environment`](#environment)
(including using [`sys.frame`](#sysframe) to access the currently active
function calls). By default, the environment of the call to `ls` or
`objects` is used. The `pos` and `envir` arguments are an alternative
way to specify an environment, but are primarily there for back
compatibility.

Note that the *order* of strings for `sorted = TRUE` is locale
dependent, see [`Sys.getlocale`](#sysgetlocale). If `sorted =` the order
is arbitrary, depending if the environment is hashed, the order of
insertion of objects, list().

### Value

either character vector of object names or object of class "ls_str"
(returned invisibly if `print` is `TRUE` ).

### Seealso

[`lsf.str`](#lsfstr) [`ls`](#ls)

## `merge.rec`

Merge list of data.frames

### Description

Recursively merge list of data frames (or data.tables - any structure
that has `merge` method)

### Usage

``` r
list(list("merge"), list("rec"))(.list, .make.sfx = F, ...)
```

### Arguments

| Argument    | Description                                                                                                                                                                                                                                                                                        |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `.list`     | List of data.frames                                                                                                                                                                                                                                                                                |
| `.make.sfx` | logical. Should the `.list` names be used as suffixes of column names for the merged data.frame? This only works with data.frame/data.table and suffixes will be added to all columns (unless specified by `by, by.x, by.y` ) - unlike real [merge](#merge) which only adds to duplicated columns. |
| `list()`    | parameters to pass to [merge](#merge)                                                                                                                                                                                                                                                              |

### Value

merged data.frame

### Examples

``` r
dt1 <- data.frame(A = letters[1:10], B = 1:10)
dt2 <- data.frame(A = letters[5:14], B = 1:10)
dt3 <- data.frame(A = letters[10:19], B = 1:10)

merge.rec(.list = list(dt1, dt2, dt3), all.x=T)
##    A  B
## 1  a  1
## 2  b  2
## 3  c  3
## 4  d  4
## 5  e  5
## 6  f  6
## 7  g  7
## 8  h  8
## 9  i  9
## 10 j 10
merge.rec(.list = list(dt1, dt2, dt3), all.y=T)
##    A  B
## 1  j  1
## 2  k  2
## 3  l  3
## 4  m  4
## 5  n  5
## 6  o  6
## 7  p  7
## 8  q  8
## 9  r  9
## 10 s 10
## arbitrary suffixes
merge.rec(.list = list(dt1, dt2, dt3), by='A')
##   A B.x B.y B
## 1 j  10   6 1
## suffixes set by list names
merge.rec(.list = list('one' = dt1, 'two' = dt2, 'three' = dt3), by='A', .make.sfx=T)
##   A B.one B.two B.three
## 1 j    10     6       1
```

## `mingtzero`

Get minimum value greater than 0

### Usage

``` r
mingtzero(x)
```

### Arguments

| Argument | Description                       |
|----------|-----------------------------------|
| `x`      | R object containing only numbers. |

### Value

minimum value

## `mixedarrange`

Order data frame by its columns

### Description

Works like plyr:: [arrange](#arrange) except uses gtools::
[mixedsort](#mixedsort) instead of plain [`order`](#order) . This allows
for embedded numbers to be sorted numerically instead of as characters.

### Usage

``` r
mixedarrange(df, ..., decreasing = FALSE, na.last = TRUE,
  blank.last = FALSE, numeric.type = "decimal", roman.case = "upper")
```

### Arguments

| Argument       | Description                                                                                                                                                                |
|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `df`           | data frame                                                                                                                                                                 |
| `list()`       | expressions evaluated in the context of df and then fed to mixedsort and order. usually column names.                                                                      |
| `decreasing`   | logical vector. Should the sorting be decreasing? See Details for length.                                                                                                  |
| `na.last`      | parameter for mixedsort for controlling the treatment of NA values. If TRUE, missing values in the data are put last; if FALSE, they are put first                         |
| `blank.last`   | for controlling the treatment of blank values. If TRUE, blank values in the data are put last; if FALSE, they are put first                                                |
| `numeric.type` | vector. either "decimal" (default) or "roman". Are numeric values represented as decimal numbers or as Roman numerals? See Details for length.                             |
| `roman.case`   | vector. one of "upper", "lower", or "both". Are roman numerals represented using only capital letters ('IX') or lower-case letters ('ix') or both? See Details for length. |

### Details

Parameters `decreasing`, `blank.last`, `numeric.type`, and `roman.case`
are passed to mixedsort. If they are length 1, then they are applied the
same way for all elements of ... Otherwise, the vectors should be the
same length as the number of columns passed.

### Value

ordered dataframe

### Examples

``` r
Treatment <- c("Control", "Aspirin 10mg/day", "Aspirin 50mg/day",
"Aspirin 100mg/day", "Acetominophen 1000mg/day", "Acetominophen 1000mg/day")
chapters <- c("V. Non Sequiturs", "II. More Nonsense",
"I. Nonsense", "IV. Nonesensical Citations", "III. Utter Nonsense",
"vi. Nunsense")
df <- data.frame(Treatment, chapters)

## sort with both columns decreasing
mixedarrange(df, Treatment, chapters, decreasing=T)
##                  Treatment                   chapters
## 1                  Control           V. Non Sequiturs
## 4        Aspirin 100mg/day IV. Nonesensical Citations
## 3         Aspirin 50mg/day                I. Nonsense
## 2         Aspirin 10mg/day          II. More Nonsense
## 6 Acetominophen 1000mg/day               vi. Nunsense
## 5 Acetominophen 1000mg/day        III. Utter Nonsense

## sort with both columns one decreasing, one increasing
mixedarrange(df, Treatment, chapters, decreasing=c(T, F))
##                  Treatment                   chapters
## 1                  Control           V. Non Sequiturs
## 4        Aspirin 100mg/day IV. Nonesensical Citations
## 3         Aspirin 50mg/day                I. Nonsense
## 2         Aspirin 10mg/day          II. More Nonsense
## 5 Acetominophen 1000mg/day        III. Utter Nonsense
## 6 Acetominophen 1000mg/day               vi. Nunsense
```

## `namedlist`

named lists and vectors

### Description

Make list or vector from input variables and set names of the resulting
object to be the input variable names.

### Usage

``` r
namedlist(...)
namedvector(...)
```

### Arguments

| Argument | Description                                         |
|----------|-----------------------------------------------------|
| `list()` | variables to concatenate or from which to form list |

### Value

named list or vector

### Note

`namedvector` uses `c` to concatenate, so input objects should all be of
dimension 1. See examples.

### References

"R - Can Lists Be Created That Name Themselves Based on Input Object
Names?" *Stack Overflow*,
<https://stackoverflow.com/questions/16951080/can-lists-be-created-that-name-themselves-based-on-input-object-names#comment24478616_16951151>.
Accessed 26 Aug. 2021.

### Examples

``` r
lets <- LETTERS[1:4]
a <- 55
namedlist(lets, a)
## $lets
## [1] "A" "B" "C" "D"
## 
## $a
## [1] 55

# uh oh
namedvector(lets, a)
## Warning in namedvector(lets, a): some names may be incorrect or NA as not all objects are of dim 1
## lets    a <NA> <NA> <NA> 
##  "A"  "B"  "C"  "D" "55"
```

## `printdebug`

Print objects for debugging

### Usage

``` r
printdebug(x, message = F)
```

### Arguments

| Argument  | Description                                                                |
|-----------|----------------------------------------------------------------------------|
| `x`       | object/variable to print                                                   |
| `message` | logical; generate diagnostic message (stderr) instead of printing (stdout) |

## `revcomp`

Reverse complement DNA strings

### Usage

``` r
revcomp(x, case = c(NA, "lower", "upper"))
```

### Arguments

| Argument | Description                                                                                                                                                   |
|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `x`      | character. should be DNA bp                                                                                                                                   |
| `case`   | default `NA` returns the vector with characters exactly the same case they were on input. `lower` and `upper` return lower and upper case vector respectively |

### Value

vector of reverse complemented strings

### Examples

``` r
y <- c("ACGACGTG", "ATATACAC", "CGTCGCTA")
revcomp(y)
## [1] "CACGTCGT" "GTGTATAT" "TAGCGACG"
revcomp(y, case='lower')
## [1] "cacgtcgt" "gtgtatat" "tagcgacg"
```

## `rmall`

Remove all opjects

### Description

Removes all objects in Global environment.

### Usage

``` r
rmall(cc = NULL)
```

### Arguments

| Argument | Description                                                             |
|----------|-------------------------------------------------------------------------|
| `cc`     | names of objects that should not be removed. Vector of strings (names). |

### Examples

``` r
## Not run:
 x <- 77
y <- "stringvars"
ls()
rmall(cc="x")
ls() 
## End(Not run)
```

## `setclasses`

Set classes of columns in data.table or data.frame

### Usage

``` r
setclasses(dt, cols, fac2char = TRUE)
```

### Arguments

| Argument   | Description                                                                                                                                                             |
|------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `dt`       | data.table or data.frame                                                                                                                                                |
| `cols`     | named list of vectors of columns to change class names of vectors should be the class. See example [as](#as) . Can specify columns as names or index/column number.     |
| `fac2char` | if `TRUE` , convert factors to characters first before converting. Useful if values are numbers and converting to numeric (rather than converting level ranks numbers). |

### Value

If dt is data.frame, returns data.frame. If dt is data.table, returns
data.table invisibly, as *it will modify the data.table globally.* This
is similar to other data.table functions like [`setnames`](#setnames) or
[`setcolorder`](#setcolorder) .

### Examples

``` r
data("mtcars")

lapply(mtcars, class)
## $mpg
## [1] "numeric"
## 
## $cyl
## [1] "numeric"
## 
## $disp
## [1] "numeric"
## 
## $hp
## [1] "numeric"
## 
## $drat
## [1] "numeric"
## 
## $wt
## [1] "numeric"
## 
## $qsec
## [1] "numeric"
## 
## $vs
## [1] "numeric"
## 
## $am
## [1] "numeric"
## 
## $gear
## [1] "numeric"
## 
## $carb
## [1] "numeric"
new <- setclasses(mtcars, cols = list(factor = c('gear', 'am'),
character = c('drat', 'qsec')))
lapply(new, class)
## $mpg
## [1] "numeric"
## 
## $cyl
## [1] "numeric"
## 
## $disp
## [1] "numeric"
## 
## $hp
## [1] "numeric"
## 
## $drat
## [1] "character"
## 
## $wt
## [1] "numeric"
## 
## $qsec
## [1] "character"
## 
## $vs
## [1] "numeric"
## 
## $am
## [1] "factor"
## 
## $gear
## [1] "factor"
## 
## $carb
## [1] "numeric"
```

## `tdf`

Transpose data frame

### Description

The [`t`](#t) operator coerces data frames to matrices and sometimes
loses row names which is less than ideal. This preserves row names and
can add the row names as a column and returns a data frame.

### Usage

``` r
tdf(df, rname = "var", row2col = F)
```

### Arguments

| Argument  | Description                                                                                                                                                       |
|-----------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `df`      | data frame                                                                                                                                                        |
| `rname`   | name of the row name column                                                                                                                                       |
| `row2col` | put the row names in a column. if FALSE, will return data frame with actual row names and no row name column. if NULL, no row name column and row.names are NULL. |

### Value

transposed data frame

## `vswitch`

Vectorized switch

### Description

Uses [`Vectorize`](#vectorize) to vectorize [`switch`](#switch) .

### Usage

``` r
vswitch(EXPR, ..., default = NULL, USE.NAMES = TRUE, SIMPLIFY = TRUE)
```

### Arguments

| Argument    | Description                                                                                                                                                                 |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `EXPR`      | an expression evaluating to a numeric or a character vector.                                                                                                                |
| `...`       | the list of alternatives. If it is intended that `EXPR` has a character-string value these will be named, perhaps except for one alternative to be used as a default value. |
| `default`   | default value for numeric forms (see examples/details - this is not needed for character-string form)                                                                       |
| `USE.NAMES` | logical; use names if `EXPR` has names, or if it is a character vector, use that character vector as the names. Along with `SIMPLIFY` passed to [`Vectorize`](#vectorize)   |
| `SIMPLIFY`  | logical or character string; attempt to reduce the result to a vector, matrix or higher dimensional array; see the `simplify` argument of [`sapply`](#sapply) .             |

### Details

`switch` works in two distinct ways depending whether the first argument
evaluates to a character string or a number.

If the value of `EXPR` is not a character string it is coerced to
integer. Note that this also happens for [`factor`](#factor)s, with a
warning, as typically the character level is meant. If the integer is
between 1 and `nargs()-1` then the corresponding element of list() is
evaluated and the result returned: thus if the first argument is `3`
then the fourth argument is evaluated and returned.

If `EXPR` evaluates to a character string then that string is matched
(exactly) to the names of the elements in list(). If there is a match
then that element is evaluated unless it is missing, in which case the
next non-missing element is evaluated, so for example
`switch("cc", a = 1, cc =, cd =, d = 2)` evaluates to `2`. If there is
more than one match, the first matching element is used. In the case of
no match, if there is an unnamed element of list() its value is
returned. (If there is more than one such argument an error is
signaled.)

The first argument is always taken to be `EXPR`: if it is named its name
must (partially) match.

A warning is signaled if no alternatives are provided, as this is
usually a coding error.

This is implemented as a [primitive](#primitive) function that only
evaluates its first argument and one other if one is selected.

### Value

The value of one of the elements of list() , or `NULL` , invisibly
(whenever no element is selected).

The result has the visibility (see [`invisible`](#invisible) ) of the
element evaluated.

### Seealso

[`switch`](#switch) [`dplyr::recode`](#dplyr::recode)

### Note

The return value is list, vector, matrix or array depending on
`SIMPLIFY` arg - see Value section for contents/values.

### References

Becker, R. A., Chambers, J. M. and Wilks, A. R. (1988) *The New S
Language*. Wadsworth & Brooks/Cole.

### Examples

``` r
## from base switch example with default last arg
ccc <- c("b","QQ","a","A","bb")
for(ch in ccc)
cat(ch,":", switch(EXPR = ch, a =, A = 1, b = 2:3, "Otherwise: last"),"\n")
## b : 2 3 
## QQ : Otherwise: last 
## a : 1 
## A : 1 
## bb : Otherwise: last
## using vswitch
print(vswitch(EXPR = ccc, a =, A = 1, b = 2:3, "Otherwise: last"))
## $b
## [1] 2 3
## 
## $QQ
## [1] "Otherwise: last"
## 
## $a
## [1] 1
## 
## $A
## [1] 1
## 
## $bb
## [1] "Otherwise: last"

## Base switch example
## Numeric EXPR does not allow a default value to be specified
## -- it is always NULL
for(i in c(-1:3, 9))  print(switch(i, 1, 2 , 3, 4))
## NULL
## NULL
## [1] 1
## [1] 2
## [1] 3
## NULL
## contrast to vswitch
print(vswitch(EXPR = c(-1:3, 9), default = 4, 1, 2 , 3))
## [1] 4 4 1 2 3 4

## default can be a function
vswitch(LETTERS[1:5], C = 77, default = identity, USE.NAMES = F)
## [1] "A"  "B"  "77" "D"  "E"
```

### Warning

It is possible to write calls to `switch` that can be confusing and may
not work in the same way in earlier versions of list() . For
compatibility (and clarity), always have `EXPR` as the first argument,
naming it if partial matching is a possibility. For the character-string
form, have a single unnamed argument as the default after the named
values.

## `write.tab.table`

Write a data frame to file

### Description

Convenience function for writing tab separated table with my favorite
parameters.

### Usage

``` r
write.tab.table(df, filename, sep = "\t", na = "", ...)
```

### Arguments

| Argument   | Description                                                                                                    |
|------------|----------------------------------------------------------------------------------------------------------------|
| `df`       | data frame to write to file                                                                                    |
| `filename` | file to write data frame into. If file extension is '.csv', csv file will be written instead of tab separated. |
| `sep`      | field separator (default tab separated file; use ',' for csv)                                                  |
| `na`       | what to print for `NA` values.                                                                                 |
| `...`      | other parameters to pass to [`write.table`](#writetable)                                                       |

# Internal

## `meltdt`

Wrapper for multiple melting data.tables

### Description

See [`melt`](#melt) . Normally, `melt` only creates a "variable" column
with a factor 1,2,3,etc. This function creates extra columns which hold
the melted variable/column names. Obviously requires data.table to be
installed.

### Usage

``` r
meltdt(dt, measure.vars, variable.names, ...)
```

### Arguments

| Argument         | Description                                                                    |
|------------------|--------------------------------------------------------------------------------|
| `dt`             | data.table                                                                     |
| `measure.vars`   | list of measure variables to melt. If named list, value columns will be named. |
| `variable.names` | list of variable names.                                                        |
| `list()`         | other parameters passed to [`melt`](#melt)                                     |

### Details

Normally, `melt` only creates a "variable" column with a factor
1,2,3,etc that represents the tuples of columns melted. The tuples have
the same length as `measure.vars`. so for a list of measure.vars L,
variable 1 represents the first element of every list item, 2 represents
the second element of every list item. This function creates extra
columns which hold the variable/column names which form the tuples.

It is slightly dangerous, as melting multiple variables this way, the
melted values columns are tied together indexed by the `variable`
column, and we are splitting the index into 2 new variable name
columns - these columns are not independent! Think if the variable
column as the 2 new variable name columns pasted together.

### Value

melted data.table

### Note

Not exported, as not used often

## `non-exported`

Non-exported functions

### Description

These functions aren't exported because they are largely superseded
by/deprecated in favor of better ones (often in list("base") ).

### Function Help

- [`splice`](#splice)

- [`sourcepart`](#sourcepart)

- [`unloadall`](#unloadall)

- [`uses.installed`](#usesinstalled)

## `poorani-package`

Poorani's Helpful Functions

### Description

These functions used to be in my .Rprofile, but I realized it would be
better to put them in a package.

### Seealso

Useful links:

- <https://gitlab.com/pooranis/pooraniRpackage>

### Author

Poorani Subramanian

## `sourcepart-function`

Source part of file

### Description

Source only part of a file based on line numbers or function name.
Superseded by [`import::here`](#import::here) if using `fname` arg...

### Usage

``` r
sourcepart(file, start = NULL, end = NULL, fname = NULL, local = F,
  ...)
```

### Arguments

| Argument | Description                                                                                                                    |
|----------|--------------------------------------------------------------------------------------------------------------------------------|
| `file`   | file to source                                                                                                                 |
| `start`  | start line                                                                                                                     |
| `end`    | end line                                                                                                                       |
| `fname`  | function name (see details)                                                                                                    |
| `local`  | logical, name of attached local environment, or environment object to source into. Default, `FALSE` , sources into Global env. |
| `...`    | other parameters to pass on to [`source`](#source)                                                                             |

### Details

You must specify *either* `start` and `end` line numbers *or* `fname` -
*not both*. If `local` is `TRUE`, then the code lines are sourced in the
calling environment - useful if you are using `sourcepart` inside of a
function and only want to source the lines of code inside the function's
environment.

Also, if you assign function to an environment in the source file, be
sure to start the line right after the function closing bracket with
`environment(fname) <- ...` to ensure that line will be sourced as well.

### Value

Value is returned invisibly. If `local` is `FALSE` , returns `FALSE` .
Otherwise, returns environment that is sourced into.

### Note

not exported because largely superseded by [`here`](#here) .

## `splice-function`

Splice vector with another vector

### Description

Form without replace is deprecated in favor of [`append`](#append) ...

### Usage

``` r
splice(orig, index, value, replace = length(value))
```

### Arguments

| Argument  | Description                                                                                                                                                                                              |
|-----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `orig`    | original vector                                                                                                                                                                                          |
| `index`   | index after which to insert values                                                                                                                                                                       |
| `value`   | vector to splice into original vector                                                                                                                                                                    |
| `replace` | integer. starting at `index` , number of elements to replace in `orig` with `value` . Default is length of `value` . To not replace (splicing and preserving original elts on either side), set to `0` . |

### Value

spliced vector

### Note

Not exported because of redundancy with [`append`](#append) .

### Examples

``` r
ll <- letters[3:5]
poorani:::splice(LETTERS[1:10], 3, ll)
##  [1] "A" "B" "c" "d" "e" "F" "G" "H" "I" "J"
## this is same as base::append - note order of args; when splice was written,
## didn't know about append, so order is switched :(
append(LETTERS[1:10], ll, 3)
##  [1] "A" "B" "C" "c" "d" "e" "D" "E" "F" "G" "H" "I" "J"

## replace
poorani:::splice(LETTERS[1:10], 3, ll, replace=length(ll))
##  [1] "A" "B" "c" "d" "e" "F" "G" "H" "I" "J"
```

## `stop_quietly`

Intentionally stop without error

### Description

This function uses [`stop`](#stop) , but masks error messages and in
non-interactive session, by default reports exit code 0.

### Usage

``` r
stop_quietly(status = 0)
```

### Arguments

| Argument | Description                                               |
|----------|-----------------------------------------------------------|
| `status` | exit code to return in non-interactive session. default 0 |

### Value

Doesn't return anything

## `unloadall-function`

Unload all packages and restart R

### Description

Also detaches rmodules made by [rmodules](#rmodules) package. This
function is not exported because RStudio has an option Session -\>
Restart R which clears the NAMESPACE and attached packages/envs, but you
also have to use separate option Clear Workspace to clear the Global
env. Use `poorani:::unloadall()`

### Usage

``` r
unloadall(attonly = TRUE, rmall = TRUE, restart = FALSE,
  verbose = getOption("verbose"))
```

### Arguments

| Argument  | Description                          |
|-----------|--------------------------------------|
| `attonly` | unload attached packages or all      |
| `rmall`   | remove all objects from environment? |
| `restart` | restart R?                           |
| `verbose` | print some messages                  |

### Note

Usually it is easier to use [rmall](#rmall) and then just restart R (in
RStudio `Cmd+Shift+0` ), so this function is not exported.

## `uses.installed-function`

Get which installed packages depend on a package

### Description

Superseded by [`tools::dependsOnPkgs()`](#tools::dependsonpkgs()) which
is superior as it searches for deps recursively.

### Usage

``` r
uses.installed(pkg, full = T, deps = c("Depends", "Imports", "LinkingTo"))
```

### Arguments

| Argument | Description                                                                         |
|----------|-------------------------------------------------------------------------------------|
| `pkg`    | character. Package to check in other packages dependencies                          |
| `full`   | logical. Return full packages data frame. False just return vector of package names |
| `deps`   | character. Subset of `c("Depends", "Imports", "LinkingTo", "Suggests", "Enhances")` |

### Details

Not exported. Use as `poorani:::uses.installed(pkg)`

### Value

If `full` is FALSE, return a character vector of names of packages which
depend (based on `deps` ) on `pkg` . If `full` is TRUE, returns the full
dataframe with information about the packages which depend on `pkg`
