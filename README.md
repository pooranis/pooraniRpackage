
# poorani R package

<!-- badges: start -->
<!-- badges: end -->

The goal of poorani is to put all of my helper functions in a nice package.

- You can install poorani from [gitlab](https://gitlab.com/pooranis/pooraniRpackage) with:

``` r
remotes::install_gitlab("pooranis/pooraniRpackage")
```

- [Package/function docs](doc/Reference_Manual_poorani.md)


